import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/main.css';
import './assets/css/addButton.css';
//import {fetchHandmadeData} from "./js/handmade/fetchHandmadeData"

export const eventBus = new Vue();

Vue.config.productionTip = true;

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')




// TODO Das Folgende in eine eigene js Datei packen

// YouTube: When the IFrame player loads this methods gets called by someone or something, i dont know
window.onYouTubeIframeAPIReady = function () {
  // eslint-disable-next-line no-undef
  Vue.prototype.$player = new YT.Player('you-tube-player', { //Wird in YouTube Cmponent verwendet
    width: "560",
    height: "315",
    videoId: 'NpEaa2P7qZI',
    events: {
      'onReady': function () {
        //eventBus.$emit("play-next-video");
      },
      'onStateChange': function (event) {
        // eslint-disable-next-line no-undef
        if (event.data === YT.PlayerState.ENDED) {
          eventBus.$emit("video-ended");
        }
      }
    }
  });
}
// YouTube: This code loads the IFrame Player API code asynchronously
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



// Per Hand getipptest Json in Datenbank hochladen

//fetchHandmadeData();


