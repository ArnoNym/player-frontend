import Vuex from 'vuex';
import Vue from 'vue';
import player from './modules/player';
import collections from './modules/collections';
import randomizer from "./modules/randomizer";
import others from "./modules/others";
import user from "./modules/user";
import socket from "./modules/socket";
import {FetchTypes} from "@/js/types/fetch-types";
import {MutationTypes} from "@/js/types/mutation-types";

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
    modules: {
        player,
        collections,
        randomizer,
        others,
        user,
        socket
    },
    state: {
        testPlayer: null
    },
    mutations: {
        /*
        //TODO TEST LOESCHEN
        setPlayer: (state, player) => {
            state.testPlayer = player;
        },
        */
    },
    actions: {
        async initStore({commit, rootGetters, dispatch}) {
            const playerId = rootGetters.playerId;

            dispatch("getCollections");

            let response = await fetch(FetchTypes.GET_VIDEOS_OF_PLAYER, rootGetters.createDataByStringRequest(playerId));
            if (response.ok) {
                const videos = await response.json();
                commit(MutationTypes.SET_VIDEOS_IN_PLAYER, videos);
            } else {
                console.error("Could not fetch next videos! response.status="+response.status); //TODO Error Page
            }
        },
    }
});
