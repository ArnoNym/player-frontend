/**
 * Fuer jede aktivierte collection ein array
 * in diesem array fuer jedes video der collection ein counter
 * zusaetzlich (aus performance Gruenden) ein counter fuer die gesamte collection der immer gleich aller einzelnen counter ist
 *
 * wenn ein random video gespielt werden soll werden alle großen counter zusammengezählt und von 0 bis zu dieser Zahl eine Zufallszahl gewählt.
 * dann werden die Zahlen der collections abgezogen bis man im negativen landen wuerde
 * dann werden die Zahlen der videos abgezogen bis man im negativen landen wuerde
 * dies ist das abzuspielende video. Video rausgeben und seinen counter auf 0 setzten (und den counter der collection verringern)
 */

import VideoInfo from "@/js/classes/videoInfo";
import CollectionInfo from "@/js/classes/collectionInfo";
import {MutationTypes} from "@/js/types/mutation-types";

const state = {
    randomVideo: null,
    counterSum: 0,
    collectionInfos: [],
};

const getters = {
    getRandomVideo: state => {
        return state.randomVideo;
    },
    isCollectionActivated: (state, getters) => collectionId => {
        return getters.getCollectionInfoById(collectionId) !== null;
    },
    getCollectionInfoById: state => collectionId => {
        for (let collectionInfo of state.collectionInfos) {
            if (collectionId === collectionInfo.id) {
                return collectionInfo;
            }
        }
        return null;
    },
    getCollectionInfoVideoInfoById: (state, getters) => (collectionId, videoId) => {
        const collectionInfo = getters.getCollectionInfoById(collectionId);
        if (collectionInfo !== null) {
            for (let videoInfo of collectionInfo.videoInfos) {
                if (videoInfo.id === videoId) {
                    return {collectionInfo: collectionInfo, videoInfo: videoInfo};
                }
            }
        }
        return null;
    },
    getCollectionInfoVideoInfoByVideoId: (state) => (videoId) => {
        let collectionInfo;
        let videoInfo;
        for (collectionInfo of state.collectionInfos) {
            for (videoInfo of collectionInfo.videoInfos) {
                if (videoInfo.id === videoId) {
                    return {collectionInfo: collectionInfo, videoInfo: videoInfo};
                }
            }
        }
        return null;
    },
};

const mutations = {
    [MutationTypes.DELETE_VIDEO_INFO]: function (state, {collectionInfo, videoInfo}) {
        const index = collectionInfo.videoInfos.indexOf(videoInfo);
        if (index < 0) {
            return;
        }
        if (state.randomVideo != null && state.randomVideo.id === videoInfo.id) {
            state.randomVideo = null;
        }
        collectionInfo.videoInfos.splice(index, 1);
        const counterDiff = videoInfo.counter;
        collectionInfo.counter -= counterDiff;
        state.counterSum -= counterDiff;
    },
    [MutationTypes.DELETE_COLLECTION_INFO]: function (state, {collectionInfo}) {
        const index = state.collectionInfos.indexOf(collectionInfo);
        if (index > -1) {
            state.collectionInfos.splice(index, 1);
        }
        state.counterSum -= collectionInfo.counter;
    },
    [MutationTypes.SET_RANDOM_VIDEO]: function (state, randomVideo) {
        state.randomVideo = randomVideo;
    },
    [MutationTypes.INCREMENT_ALL_COUNTERS]: function (state) {
        for (let collectionInfo of state.collectionInfos) {
            for (let videoInfo of collectionInfo.videoInfos) {
                videoInfo.counter += 1;
            }
            const collectionCounterIncrement = collectionInfo.videoInfos.length;
            collectionInfo.counter += collectionCounterIncrement;
            state.counterSum += collectionCounterIncrement;
        }
    },
    [MutationTypes.RESET_VIDEO_COUNTERS]: function(state, {collectionInfo, videoInfo}) {
        const counterResetToValue = 1;
        const counterDiff = videoInfo.counter - counterResetToValue;

        collectionInfo.counter -= counterDiff;
        videoInfo.counter = counterResetToValue;
        state.counterSum -= counterDiff;
    },
    [MutationTypes.ADD_COLLECTION_TO_RANDOMIZER]: function(state, collection) {
        const videoInfos = [];
        for (let video of collection.videos) {
            videoInfos.push(new VideoInfo(video.id, 1));
        }

        const collectionInfo = new CollectionInfo(collection.id, videoInfos.length, videoInfos);

        state.counterSum += collectionInfo.counter;

        state.collectionInfos.push(collectionInfo);
    },
    [MutationTypes.ADD_VIDEO_TO_RANDOMIZER]: function(state, {collectionInfo, video}) {
        collectionInfo.videoInfos.push(new VideoInfo(video.id, 1));
        collectionInfo.counter += 1;
        state.counterSum += 1;
    },
};

const actions = {
    async toggleCollection({commit}, collection) {
        for (let collectionInfo of state.collectionInfos) {
            if (collectionInfo.id === collection.id) {
                commit(MutationTypes.DELETE_COLLECTION_INFO, {collectionInfo});
                return;
            }
        }
        commit(MutationTypes.ADD_COLLECTION_TO_RANDOMIZER, collection);
    },
    async addCollectionToRandomizer({commit}, collection) {
        commit(MutationTypes.ADD_COLLECTION_TO_RANDOMIZER, collection);
    },
    async nextRandomVideo({commit, rootGetters}) {
        if (state.counterSum === 0) {
            commit(MutationTypes.SET_RANDOM_VIDEO, null);
            return;
        }

        let index = Math.floor((Math.random() * state.counterSum));

        let collectionInfo;
        for (collectionInfo of state.collectionInfos) {
            if (index >= collectionInfo.counter) {
                index -= collectionInfo.counter;
            } else {
                break;
            }
        }
        let videoInfo;
        for (videoInfo of collectionInfo.videoInfos) {
            if (index >= videoInfo.counter) {
                index -= videoInfo.counter;
            } else {
                break;
            }
        }

        const video = rootGetters.getVideoByCollectionIdAndVideoId(collectionInfo.id, videoInfo.id);
        if (video == null) {
            commit(MutationTypes.DELETE_VIDEO_INFO, collectionInfo, videoInfo);
            await this.nextRandomVideo(commit);
        } else {
            commit(MutationTypes.INCREMENT_ALL_COUNTERS);
            commit(MutationTypes.RESET_VIDEO_COUNTERS, {collectionInfo, videoInfo});
            commit(MutationTypes.SET_RANDOM_VIDEO, video);
        }
    },
    async resetRandomCounter({commit, getters}, videoId) {
        const collectionInfoVideoInfo = getters.getCollectionInfoVideoInfoByVideoId(videoId);
        if (collectionInfoVideoInfo != null) {
            commit(MutationTypes.RESET_VIDEO_COUNTERS,
                {collectionInfo: collectionInfoVideoInfo.collectionInfo, videoInfo: collectionInfoVideoInfo.videoInfo});
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
