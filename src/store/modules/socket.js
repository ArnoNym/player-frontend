import {MutationTypes} from "@/js/types/mutation-types";
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";
import {DATABASE_WEBSOCKET_URL} from "@/constants";
import {createDestination, SubscribeTypes} from "@/js/types/subscribe-types";

const state = {
    stompClient: null,
    connected: false
};

const getters = {
    stompClient: state => state.stompClient,
    connected: state => state.connected,
    createDestination: (state, rootGetters) => subscribeType => createDestination(subscribeType, rootGetters.subscribeToken),
};

const mutations = {
    [MutationTypes.SET_STOMP_CLIENT]: (state, stompClient) => {
        state.stompClient = stompClient;
    },
    [MutationTypes.SET_CONNECTED]: (state, connected) => {
        state.connected = connected;
    },
};

const actions = {
    async connect({commit, rootGetters}) {
        if (!rootGetters.loggedIn) {
            console.error("You have to log in before a socket connection may be established!");
            return;
        }
        const accessToken = rootGetters.accessToken;

        const socket = await new SockJS(DATABASE_WEBSOCKET_URL);

        const stompClient = await Stomp.over(socket);

        stompClient.connect(
            {
                // Das ist nicht der uebliche http header sondern ein in der stomp connection versteckter
                'x-auth-token': accessToken
            },
            frame => {
                console.log(frame);

                commit(MutationTypes.SET_CONNECTED, true);
                commit(MutationTypes.SET_STOMP_CLIENT, stompClient);

                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.ADD_COLLECTION_TO_PLAYER), tick => {
                    const collection = JSON.parse(tick.body);
                    commit(MutationTypes.ADD_COLLECTION_TO_COLLECTIONS, collection);
                });
                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.REMOVE_COLLECTION_FROM_PLAYER), tick => {
                    const playerIdCollectionIdWrapper = JSON.parse(tick.body);
                    commit(MutationTypes.REMOVE_COLLECTION_FROM_COLLECTIONS, playerIdCollectionIdWrapper.collectionId);
                    const collectionInfo = rootGetters.getCollectionById(playerIdCollectionIdWrapper.collectionId);
                    if (collectionInfo != null) {
                        commit(MutationTypes.DELETE_COLLECTION_INFO, {collectionInfo}, {root: true});
                    }

                });

                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.ADD_VIDEO_TO_COLLECTION), tick => {
                    const video = JSON.parse(tick.body);
                    commit(MutationTypes.ADD_VIDEO_TO_COLLECTIONS, video);
                });
                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.REMOVE_VIDEO_FROM_COLLECTION), tick => {
                    const collectionIdVideoIdWrapper = JSON.parse(tick.body);
                    commit(MutationTypes.REMOVE_VIDEO_FROM_COLLECTIONS, collectionIdVideoIdWrapper);
                });

                // eslint-disable-next-line no-unused-vars
                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.ADD_VIDEO_TO_PLAYER), tick => {
                    //const video = JSON.parse(tick.body);
                    // todo commit(MutationTypes.ADD_VIDEO_TTO_COLLECTIONS, video);
                });
                // eslint-disable-next-line no-unused-vars
                stompClient.subscribe(rootGetters.createDestination(SubscribeTypes.REMOVE_VIDEO_FROM_PLAYER), tick => {
                    //const video = JSON.parse(tick.body);
                    // todo commit(MutationTypes.ADD_VIDEO_TTO_COLLECTIONS, video);
                });
            },
            error => {
                console.log(error);
                commit(MutationTypes.SET_CONNECTED, false);
            }
        );
    },
    async disconnect({commit, getters}) {
        if (getters.stompClient) {
            getters.stompClient.disconnect();
        }
        commit(MutationTypes.SET_CONNECTED, false);
    },
    async toggleConnection({getters, dispatch}) {
        getters.connected ? dispatch.disconnect() : dispatch.connect();
    },
    async send(message) {
        console.log("Send message:" + message);
        if (this.stompClient && this.stompClient.connected) {
            const msg = { name: message };
            console.log(JSON.stringify(msg));
            this.stompClient.send("/app/hello", JSON.stringify(msg), {});
        }
    },
    /*
    async subscribe({getters}, channel) {
        sendMe
        //Subscribe to the channel
        getters.connection.send(JSON.stringify({
            command: "subscribe",
            identifier: "{\"channel\":\"" + channel + "\"}"
        }));
    }
    */
};

export default {
    state,
    getters,
    actions,
    mutations
};
