import {MutationTypes} from "@/js/types/mutation-types";
import {CONTROL_LOGIN, CONTROL_REGISTER} from "@/constants";
import {
    LOCAL_STORAGE_ACCESS_TOKEN_KEY, LOCAL_STORAGE_PLAYER_ID,
    LOCAL_STORAGE_PLAYER_NAME,
    LOCAL_STORAGE_SUBSCRIBE_TOKEN_KEY
} from "@/constants";

const state = {
    accessToken: localStorage.getItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY) || null,
    subscribeToken: localStorage.getItem(LOCAL_STORAGE_SUBSCRIBE_TOKEN_KEY) || null,
    playerId: localStorage.getItem(LOCAL_STORAGE_PLAYER_ID) || null,
    playerName: localStorage.getItem(LOCAL_STORAGE_PLAYER_NAME) || null,
};

const getters = {
    accessToken: state => state.accessToken,
    loggedIn: state => state.accessToken != null,

    subscribeToken: state => state.subscribeToken,

    playerId: state => state.playerId,

    playerName: state => state.playerName,

    createBody: () => data => {
        return JSON.stringify(data);
    },
    createDataByObjectRequest: (state, getters) => (object) => {
        return getters.createRequest(true, true, true, object);
    },
    createDataByStringRequest: (state, getters) => (string) => {
        return getters.createRequest(true, true, false, string);
    },
    createRequest: (state, getters) => (
        postBoolean, // if false get. Post bedeutet es wird nicht ind er URL angezeigt. Get wird angezeigt
        noCorsBoolean, // if false corse. Wenn corse sind Zugriffe von anderen URLs aus verboten localhost:8080 darf dann nur auf localhost:8080 zugreifen
        objectBoolean, // if false content ist expected to be a string
        content
    ) => {
        const request = {
            method: postBoolean ? "POST" : "GET",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: objectBoolean ? getters.createBody(content) : content
        }
        if (noCorsBoolean) {
            request["node"] = "no-cors";
        }
        if (getters.accessToken != null) {
            request["headers"]["Authorization"] = getters.accessToken;
        }
        console.log(request)
        return request;
    }
};

const mutations = {
    [MutationTypes.SET_PLAYER_ID]: function (state, playerId) {
        if (playerId == null) localStorage.removeItem(LOCAL_STORAGE_PLAYER_ID);
        else localStorage.setItem(LOCAL_STORAGE_PLAYER_ID, playerId);
        state.playerId = playerId;
    },
    [MutationTypes.SET_PLAYER_NAME]: function (state, playerName) {
        if (playerName == null) localStorage.removeItem(LOCAL_STORAGE_PLAYER_NAME);
        else localStorage.setItem(LOCAL_STORAGE_PLAYER_NAME, playerName);
        state.playerName = playerName;
    },
    [MutationTypes.SET_ACCESS_TOKEN]: function (state, accessToken) {
        if (accessToken == null) {
            localStorage.removeItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY);
        } else {
            localStorage.setItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY, accessToken);
        }
        state.accessToken = accessToken;
    },
    [MutationTypes.SET_SUBSCRIBE_TOKEN]: function (state, subscribeToken) {
        if (subscribeToken == null) {
            localStorage.removeItem(LOCAL_STORAGE_SUBSCRIBE_TOKEN_KEY);
        } else {
            localStorage.setItem(LOCAL_STORAGE_SUBSCRIBE_TOKEN_KEY, subscribeToken);
        }
        state.subscribeToken = subscribeToken;
    },
};

const actions = {
    async login({commit, getters, dispatch}, {userName, password}) {
        const request = getters.createRequest(true, false, true, {userName, password});
        const response = await fetch(CONTROL_LOGIN, request);
        if (response.ok) {
            const content = await response.json();

            const playerId = content.playerId;
            const playerName = content.playerName;
            const accessToken = content.accessToken;
            const subscribeToken = content.subscribeToken;

            console.log("accessToken=")
            console.log(accessToken)
            console.log("subscribeToken=")
            console.log(subscribeToken)
            commit(MutationTypes.SET_PLAYER_ID, playerId);
            commit(MutationTypes.SET_PLAYER_NAME, playerName);
            commit(MutationTypes.SET_ACCESS_TOKEN, accessToken);
            commit(MutationTypes.SET_SUBSCRIBE_TOKEN, subscribeToken);
            /*
            commit(MUTATION_DELETE_VIDEO, {collectionId, videoId});
            const collectionInfoVideoInfo = rootGetters.getCollectionInfoVideoInfoById(collectionId, videoId);
            if (collectionInfoVideoInfo != null) {
                commit(MUTATION_DELETE_VIDEO_INFO, collectionInfoVideoInfo, {root: true}); //In Randomizer
            }
            */
            dispatch("init");
        }
        return response;
    },
    async logout({commit, dispatch}) {
        dispatch("disconnect");
        commit(MutationTypes.SET_PLAYER_ID, null);
        commit(MutationTypes.SET_PLAYER_NAME, null);
        commit(MutationTypes.SET_ACCESS_TOKEN, null);
        commit(MutationTypes.SET_SUBSCRIBE_TOKEN, null);
        //TODO Beende TCP Verbindung
        // TODO Wipe Alles
    },
    async register({commit, getters, dispatch}, {email, userName, password}) {
        email = email == null ? "" : email;
        const request = getters.createRequest(true, false, true, {email, userName, password});
        const response = await fetch(CONTROL_REGISTER, request);
        if (response.ok) {
            const content = await response.json();
            const playerId = content.playerId;
            const playerName = content.playerName;
            const accessToken = content.accessToken;
            const subscribeToken = content.subscribeToken;
            console.log("accessToken=")
            console.log(accessToken)
            console.log("subscribeToken=")
            console.log(subscribeToken)
            commit(MutationTypes.SET_PLAYER_ID, playerId);
            commit(MutationTypes.SET_PLAYER_NAME, playerName);
            commit(MutationTypes.SET_ACCESS_TOKEN, accessToken);
            commit(MutationTypes.SET_SUBSCRIBE_TOKEN, subscribeToken);
            dispatch("init");
        }
        return response;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
