import {MUTATION_SET_SHOW_COLLECTION_ID} from "@/js/types/mutation-types";
import {RouteTypes} from "@/js/types/route-types";
import router from '../../router/index'

const state = {
    shownCollectionId: null,
    // Hier koennte man auch das aktuelle laufende Video mit Zeitstempel etc speichern
};

const getters = {
    shownCollectionId: state => state.shownCollectionId,
    isCollectionShown: (state) => collectionId => collectionId === state.shownCollectionId,
};

const actions = {
    async setShowCollectionId({commit}, collectionId) {
        commit(MUTATION_SET_SHOW_COLLECTION_ID, collectionId);
    },
    async init({rootGetters, dispatch}) {
        if (rootGetters.loggedIn) {
            dispatch("connect");
            dispatch("initStore");
        } else {
            if (router.name !== RouteTypes.LOGIN) {
                await router.push({name: RouteTypes.LOGIN}); // TODO Came-From irgendwo im Store speichern und dann hier einfuegen
            }
        }
    },
};

const mutations = {
    [MUTATION_SET_SHOW_COLLECTION_ID]: (state, collectionId) => {
        state.shownCollectionId = collectionId;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
