import {FetchTypes} from "@/js/types/fetch-types";
import {MutationTypes} from "@/js/types/mutation-types";

const state = {
    videos: []
};

const getters = {
    nxVideos: state => state.videos,
    nxVideosCount: state => state.videos.length,
    nxVideo: state => {
        if (state.videos.length > 0) {
            return state.videos[0]
        } else {
            return null;
        }
    },
};

const mutations = {
    [MutationTypes.SET_VIDEOS_IN_PLAYER]: (state, videos) => {
        state.videos = videos;
    },
    [MutationTypes.SWITCH_VIDEOS_IN_PLAYER]: (state, { index1, index2 }) => {
        const video1 = state.videos[index1];
        state.videos[index1] = state.videos[index2];
        state.videos[index2] = video1;
        state.videos.push(); // Needed so that vue understands that something changed
    },
    [MutationTypes.REMOVE_VIDEO_FROM_PLAYER]: (state, index) => {
        return state.videos.splice(index, 1);
    },
    [MutationTypes.SET_TOP_VIDEOS_IN_PLAYER]: (state, index) => {
        const video = state.videos.splice(index, 1)[0];
        state.videos.unshift(video);
    },
    [MutationTypes.SET_AS_TOP_VIDEOS_IN_PLAYER]: (state, video) => {
        state.videos.unshift(video);
    },
    [MutationTypes.SET_BOTTOM_VIDEOS_IN_PLAYER]: (state, index) => {
        const video = state.videos.splice(index, 1)[0];
        state.videos.push(video);
    },
    [MutationTypes.SET_AS_BOTTOM_VIDEOS_IN_PLAYER]: (state, video) => {
        state.videos.push(video);
    },
};

const actions = {
    async removeVideoFromPlayer({commit, getters, rootGetters}, index) {
        const playerId = rootGetters.playerId;
        const videoId = getters.nxVideos[index].id;
        const response = await fetch(FetchTypes.REMOVE_VIDEO_FROM_PLAYER, rootGetters.createDataByObjectRequest({id1: playerId, id2:videoId}));
        if (response.ok) {
            commit(MutationTypes.REMOVE_VIDEO_FROM_PLAYER, index);
        } else {
            alert(response.status); //TODO Error Page
        }
    },
    /*
    async setCurrentPlayerVideo({commit}, video) {
        //todo
    },
    */
    async switchNxVideos({ commit }, { index1, index2 }) {
        commit(MutationTypes.SWITCH_VIDEOS_IN_PLAYER, { index1: index1, index2: index2 });
    },
    async setTopPlayerVideo({ commit }, index) {
        commit(MutationTypes.SET_TOP_VIDEOS_IN_PLAYER, index);
    },
    async setAsTopPlayerVideo({ commit }, video) {
        commit(MutationTypes.SET_AS_TOP_VIDEOS_IN_PLAYER, video);
    },
    async setBottomNxVideo({ commit }, index) {
        commit(MutationTypes.SET_BOTTOM_VIDEOS_IN_PLAYER, index);
    },
    async setAsBottomNxVideo({ commit }, video) {
        commit(MutationTypes.SET_AS_BOTTOM_VIDEOS_IN_PLAYER, video);
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
