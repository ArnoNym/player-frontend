import {MutationTypes} from "@/js/types/mutation-types";
import {
    FetchTypes,
    FETCH_ADD_VIDEO,
    FETCH_DELETE_VIDEO,
} from "@/js/types/fetch-types";

const state = {
    collections: []
};

const getters = {
    getCollectionsCount: state => state.collections.length,
    getCollections: state => state.collections,
    getCollectionById: state => collectionId => state.collections.find(collection => collection.id === collectionId),
    getVideoByCollectionIdAndVideoId: (state, getters) => (collectionId, videoId) => {
        let collection = getters.getCollectionById(collectionId);
        if (collection == null) return null;
        return collection.videos.find(video =>  video.id === videoId);
    }
};

const mutations = {
    [MutationTypes.REMOVE_VIDEO_FROM_COLLECTIONS]: (state, {collectionId, videoId}) => {
        const collection = state.collections.find(collection => collection.id === collectionId);
        collection.videos = collection.videos.filter(video => video.id !== videoId);
        state.collections.push(); // To trigger update
    },
    [MutationTypes.ADD_VIDEO_TO_COLLECTIONS]: (state, {collection, video}) => {
        collection.videos.push(video);
        state.collections.push(); // To trigger update
    },
    [MutationTypes.REMOVE_COLLECTION_FROM_COLLECTIONS]: (state, collectionId) => {
        state.collections = state.collections.filter(collection => collection.id !== collectionId);
    },
    [MutationTypes.ADD_COLLECTION_TO_COLLECTIONS]: (state, {collection}) => {
        state.collections.push(collection);
    },
    [MutationTypes.SET_COLLECTIONS]: (state, collections) => {
        state.collections = collections;
    },
};

const actions = {
    async newCollection({rootGetters}, collection) {
        console.log(collection);
        const playerId = rootGetters.playerId;
        console.log("playerId="+playerId)
        const response = await fetch(FetchTypes.NEW_COLLECTION_TO_PLAYER, rootGetters.createDataByObjectRequest({playerId, collection}));
        if (!response.ok) {
            console.error(response.status); //TODO Error Page
        }
    },
    async addCollection({rootGetters}, collectionId) {
        const playerId = rootGetters.playerId;
        const response = await fetch(FetchTypes.ADD_COLLECTION_TO_PLAYER, rootGetters.createDataByObjectRequest({playerId, collectionId}));
        if (!response.ok) {
            console.error(response.status); //TODO Error Page
        }
    },
    async getCollections({commit, rootGetters}) {
        console.log("CALLED getCollections")
        const playerId = rootGetters.playerId;
        let response = await fetch(FetchTypes.GET_COLLECTIONS_OF_PLAYER, rootGetters.createDataByStringRequest(playerId));
        if (response.ok) {
            const collections = await response.json();
            console.log("collections");
            console.log(collections);
            commit(MutationTypes.SET_COLLECTIONS, collections);
        } else {
            console.error("Could not fetch collections! response.status="+response.status); //TODO Error Page
        }
    },
    async removeCollection({rootGetters}, collectionId) {
        const playerId = rootGetters.playerId;
        const response = await fetch(FetchTypes.REMOVE_COLLECTION_FROM_PLAYER, rootGetters.createDataByObjectRequest({playerId, collectionId}));
        if (response.ok) {
            /*
            commit(MutationTypes.REMOVE_COLLECTION_FROM_COLLECTIONS, {collectionId});
            const collectionInfo = rootGetters.getCollectionById(collectionId);
            if (collectionInfo != null) {
                commit(MutationTypes.DELETE_COLLECTION_INFO, {collectionInfo}, {root: true});
            }
            */
        } else {
            alert(response.status); //TODO Error Page
        }
    },
    // eslint-disable-next-line no-unused-vars
    async addActiveCollection({commit, rootGetters}, collectionId) {
        const playerId = rootGetters.playerId;
        const response = await fetch(FetchTypes.ADD_ACTIVE_COLLECTION_TO_PLAYER, rootGetters.createDataByObjectRequest({playerId, collectionId}));
        if (response.ok) {
            // eslint-disable-next-line no-unused-vars
            const collection = await response.json(); // Now the collection has an id
            //todo commit(MUTATION_ADD_COLLECTION_TO_COLLECTIONS, {collection});
        } else {
            alert(response.status); //TODO Error Page
        }
    },
    // eslint-disable-next-line no-unused-vars
    async removeActiveCollection({commit, rootGetters}, collectionId) {
        const playerId = rootGetters.playerId;
        const response = await fetch(FetchTypes.REMOVE_ACTIVE_COLLECTION_FROM_PLAYER, rootGetters.createDataByObjectRequest({playerId, collectionId}));
        if (response.ok) {
            // eslint-disable-next-line no-unused-vars
            const collection = await response.json(); // Now the collection has an id
            //todo commit(MUTATION_ADD_COLLECTION_TO_COLLECTIONS, {collection});
        } else {
            alert(response.status); //TODO Error Page
        }
    },

    async removeVideo({commit, rootGetters}, {collectionId, videoId}) {
        const response = await fetch(FETCH_DELETE_VIDEO, rootGetters.createDataByObjectRequest({id1: collectionId, id2: videoId}));//createPostRequestWithObject({id1: collectionId, id2: videoId}));
        if (response.ok) {
            commit(MutationTypes.REMOVE_VIDEO_FROM_COLLECTIONS, {collectionId, videoId});
            const collectionInfoVideoInfo = rootGetters.getCollectionInfoVideoInfoById(collectionId, videoId);
            if (collectionInfoVideoInfo != null) {
                commit(MutationTypes.DELETE_VIDEO_INFO, collectionInfoVideoInfo, {root: true}); //In Randomizer
            }
        } else {
            alert(response.status); //TODO Error Page
        }
    },
    async newVideo({commit, getters, rootGetters}, {collectionId, video}) {
        const response = await fetch(FETCH_ADD_VIDEO, rootGetters.createDataByObjectRequest({collectionId, video}));//createPostRequestWithObject({collectionId, video}));
        if (response.ok) {
            const video = await response.json(); // Now the video has an id
            const collection = getters.getCollectionById(collectionId);
            const collectionInfo = rootGetters.getCollectionInfoById(collectionId);
            if (collectionInfo != null) {
                commit(MutationTypes.ADD_VIDEO_TO_RANDOMIZER, {collectionInfo, video}, {root: true});
            }
            commit(MutationTypes.ADD_VIDEO_TO_COLLECTIONS, {collection, video});
        } else {
            alert(response.status); //TODO Error Page
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};
