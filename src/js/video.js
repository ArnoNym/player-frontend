import {Video} from "@/js/classes/video";

export const createVideo = (title, link) => {
    if (link == null) {
        alert("Link may not be empty!"); //TODO test ob echter link. Außerdem die youtube-id extrahieren!
        return;
    }
    if (title == null) {
        alert("Title may not be empty!")
        return;
    }
    const youTubeId = extractYouTubeId(link);
    if (youTubeId == null) {
        alert("Illegal link!");
        console.log("Illegal link="+link+", extractedId="+youTubeId);
        return;
    }
    return new Video("", youTubeId, title, null, null);
}

export const extractYouTubeId = function(link) {
    const equalSignIndex = link.indexOf("=");
    if (equalSignIndex === -1) {
        return null;
    }
    const andSignIndex = link.indexOf("&"); // If no & it retuns -1
    if (andSignIndex < 0) {
        return link.slice(equalSignIndex + 1);
    } else {
        return link.slice(equalSignIndex + 1, andSignIndex);
    }
}

