import Collection from "@/js/classes/collection";
import {createVideo} from "@/js/video";
import {FetchTypes, FETCH_ADD_VIDEO} from "@/js/types/fetch-types";
import handmadeData from "./handmadeData.json";
import store from '../../store';

export const fetchHandmadeData = async function() {
    await new Promise(resolve => setTimeout(resolve, 2000));

    const handmadeDataJson = handmadeData;

    const collections = [];

    for (let collectionData of handmadeDataJson) {
        const videos = [];
        for (let videoData of collectionData.videos) {
            const video = createVideo(videoData.title, videoData.link);
            if (video != null) videos.push(video);
        }
        const collection = new Collection("", collectionData.name, videos);
        collections.push(collection);
    }

    for (let collection of collections) {
        console.log("fetch collection ANFANG");
        await fetchCollection(collection);
        console.log("fetch collection ENDE");
    }
}

const fetchCollection = async (collection) => {
    const playerId = store.getters.playerId;
    const response = await fetch(FetchTypes.NEW_COLLECTION_TO_PLAYER, store.getters.createDataByObjectRequest({playerId, collection}));
    if (response.ok) {
        const responseCollection = await response.json(); // Now the collection has an id
        const collectionId = responseCollection.id; // Now the collection has an id
        for (let video of collection.videos) {
            await fetchVideo(collectionId, video);
        }
    } else {
        console.log("response.status="+response.status+" for collection=");
    }
}

const fetchVideo = async (collectionId, video) => {
    const response = await fetch(FETCH_ADD_VIDEO, store.getters.createDataByObjectRequest({collectionId, video}));
    if (!response.ok) {
        console.log("response.status="+response.status+" for video=");
        console.log(video);
    }
}
