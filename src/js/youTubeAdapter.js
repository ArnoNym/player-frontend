import {eventBus} from "@/main";

// YouTube: When the IFrame player loads this methods gets called by someone or something, i dont know
window.onYouTubeIframeAPIReady = () => {
    console.log("onYouTubeIframeAPIReady called")
    // eslint-disable-next-line no-undef
    Vue.prototype.$player = new YT.Player('you-tube-player', {
        width: "560",
        height: "315",
        videoId: 'NpEaa2P7qZI',
        events: {
            'onReady': function () {
                //eventBus.$emit("play-next-video");
            },
            'onStateChange': function (event) {
                // eslint-disable-next-line no-undef
                if (event.data === YT.PlayerState.ENDED) {
                    eventBus.$emit("video-ended");
                }
            }
        }
    });
}

// YouTube: This code loads the IFrame Player API code asynchronously
const requestYouTubeIFrame = () => {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

requestYouTubeIFrame();
