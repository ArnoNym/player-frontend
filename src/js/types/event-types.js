export const EVENT_STARTING_VIDEO = "starting-video";
const EVENT_PLAY_NEXT_VIDEO = "play-next-video";
const EVENT_PLAY_VIDEO = "play-video";
const EVENT_VIDEO_ENDED = "video-ended";

export default {
    EVENT_PLAY_NEXT_VIDEO,
    EVENT_PLAY_VIDEO,
    EVENT_VIDEO_ENDED,
}