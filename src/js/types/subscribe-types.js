
// eslint-disable-next-line no-unused-vars
const PUBLIC_PREFIX = "/topic";
const PRIVATE_PREFIX = "/queue";

export const SubscribeTypes = {
    ADD_VIDEO_TO_COLLECTION: PRIVATE_PREFIX + "/add/video/to/collection",
    ADD_VIDEO_TO_PLAYER: PRIVATE_PREFIX + "/add/video/to/player",
    ADD_COLLECTION_TO_PLAYER: PRIVATE_PREFIX + "/add/collection/to/player",

    REMOVE_VIDEO_FROM_COLLECTION: PRIVATE_PREFIX + "/remove/video/from/collection",
    REMOVE_VIDEO_FROM_PLAYER: PRIVATE_PREFIX + "/remove/video/from/player",
    REMOVE_COLLECTION_FROM_PLAYER: PRIVATE_PREFIX + "/remove/collection/from/player"
}

export const createDestination = (subscribeType, subscribeToken) => {
    return subscribeType + "/" + subscribeToken;
}
