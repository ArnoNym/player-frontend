import {DATABASE_URL} from "@/constants";

export const FETCH_TEST = DATABASE_URL+"/test";

export const FETCH_ADD_VIDEO = DATABASE_URL+"/addVideo";
export const FETCH_DELETE_VIDEO = DATABASE_URL+"/deleteVideo";

export const FetchTypes = {
    NEW_COLLECTION_TO_PLAYER: DATABASE_URL+"/newCollectionToPlayer",
    ADD_COLLECTION_TO_PLAYER: DATABASE_URL+"/addCollectionToPlayer",
    GET_COLLECTIONS_OF_PLAYER: DATABASE_URL+"/getCollectionsOfPlayer",
    REMOVE_COLLECTION_FROM_PLAYER: DATABASE_URL+"/removeCollectionFromPlayer",

    ADD_ACTIVE_COLLECTION_TO_PLAYER: DATABASE_URL+"/addActiveCollectionToPlayer",
    GET_ACTIVE_COLLECTIONS_OF_PLAYER: DATABASE_URL+"/getActiveCollectionsOfPlayer",
    REMOVE_ACTIVE_COLLECTION_FROM_PLAYER: DATABASE_URL+"/removeActiveCollectionFromPlayer",

    ADD_VIDEO_TO_PLAYER: DATABASE_URL+"/addVideoToPlayer",
    GET_VIDEOS_OF_PLAYER: DATABASE_URL+"/getVideosOfPlayer",
    REMOVE_VIDEO_FROM_PLAYER: DATABASE_URL+"/removeVideoFromPlayer",
}
