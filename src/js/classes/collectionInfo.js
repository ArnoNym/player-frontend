export default class CollectionInfo {
    constructor(id, counter, videoInfos) {
        this.id = id;
        this.counter = counter;
        this.videoInfos = videoInfos;
    }
}