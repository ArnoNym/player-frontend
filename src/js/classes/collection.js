export default class Collection {
    constructor(id, name, videos) {
        this.id = id;
        this.name = name;
        this.videos = videos;
    }
}