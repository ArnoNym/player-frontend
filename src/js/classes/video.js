export class Video {
    constructor(id, link, title, artist, description) {
        this.id = id;
        this.link = link;
        this.title = title;
        this.artist = artist;
        this.description = description;
    }
}