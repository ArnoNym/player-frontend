import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import {RouteTypes} from "@/js/types/route-types";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: RouteTypes.HOME,
    component: Home
  },
  {
    path: '/'+RouteTypes.LOGIN,
    name: RouteTypes.LOGIN,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/'+RouteTypes.REGISTER,
    name: RouteTypes.REGISTER,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
