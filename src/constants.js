// Also see fetch-types

export const DATABASE_URL = "http://localhost:8080";

export const DATABASE_WEBSOCKET_URL = DATABASE_URL + "/socket";

export const CONTROL_LOGIN = DATABASE_URL+"/authenticate";
export const CONTROL_REGISTER = DATABASE_URL+"/register";

export const LOCAL_STORAGE_ACCESS_TOKEN_KEY = "access_token";
export const LOCAL_STORAGE_SUBSCRIBE_TOKEN_KEY = "subscribe_token";
export const LOCAL_STORAGE_PLAYER_ID = "player_id";
export const LOCAL_STORAGE_PLAYER_NAME = "player_name";

/*
export const POST_TEMPLATE = {
    mode: "no-cors",
    method: "POST",
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
}
export const GET_TEMPLATE = {
    mode: "no-cors",
    method: "GET",
    headers: {
        "Content-type": "application/json; charset=UTF-8"
    }
}

export const createBody = data => {
    return JSON.stringify(data);
}

export const createRequestWithObject = (template, object) => {
    template["body"] = createBody(object);
    return template;
}
export const createRequestWithString = (template, object) => {
    template["body"] = object;
    return template;
}

export const createPostRequestWithObject = object => {
    return createRequestWithObject(POST_TEMPLATE, object);
}
export const createGetRequestWithObject = object => {
    return createRequestWithObject(GET_TEMPLATE, object);
}

export const createPostRequestWithString = string => {
    return createRequestWithString(POST_TEMPLATE, string);
}
export const createGetRequestWithString = string => {
    return createRequestWithString(GET_TEMPLATE, string);
}

*/



